package com.aiad.ui;

import javafx.scene.shape.Shape;

public interface AgentDrawable {


    public int getxPos();

    public int getyPos();

    public Shape getAgentShape();

}
