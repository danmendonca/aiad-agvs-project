package com.aiad.ui;

// JADE
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.ContainerController;
import jade.wrapper.StaleProxyException;

// JAVA
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.Random;

import com.aiad.agents.AGVAgent;
import com.aiad.agents.ManagerAgent;
import com.aiad.agents.ProcessMachineAgent;
import com.aiad.agents.StorageAgent;


public class ScenarioLauncher extends Application {

    public static final boolean DEBUG_MODE = false;

    // Project settings
    public static final boolean SEPARATE_CONTAINERS = false;

    private int WINDOW_WIDTH = 200;
    private int WINDOW_HEIGHT = 100;


    private ContainerController mainContainer;
    private ContainerController agentContainer;

    //JAVAFX
    private static ArrayList<AgentDrawable> drawList = new ArrayList<>();


    /**
     * @param args
     */
    public static void main(String[] args) {

        // Launch JADE
        new ScenarioLauncher().launchJADE(Integer.parseInt(args[0]), Integer.parseInt(args[1]),
                Boolean.parseBoolean(args[2]), Integer.parseInt(args[3]), Boolean.parseBoolean(args[4]));

        // JAVAFX
        Application.launch();

    }

    /**
     *
     * @param stage
     * @throws Exception
     */
    @Override
    public void start(Stage stage) throws Exception {

        Group root = new Group();
        Scene scene = new Scene(root, WINDOW_WIDTH, WINDOW_HEIGHT, Color.WHITE);
        stage.setTitle("AIAD - AGV: Factory Plant");
        stage.setScene(scene);

        // Recharging Station
        Circle rs = new Circle(AGVAgent.RECHARGING_X_COORDINATE, AGVAgent.RECHARGING_Y_COORDINATE,
                5, Color.RED);
        root.getChildren().add(rs);

        drawList.forEach(a -> {
            root.getChildren().add(a.getAgentShape());
        });

        stage.show();

        AnimationTimer animationLoop = new AnimationTimer() {

            @Override
            public void handle(long now) {

                drawList.forEach(a -> a.getAgentShape().relocate(a.getxPos(), a.getyPos()));

            }
        };

        animationLoop.start();

    }

    /**
     *
     */
    protected void launchJADE(int ratePart, int numAGVs, boolean ftc, int agvCap, boolean relocAGVs){

        Runtime rt = Runtime.instance();
        Profile p1 = new ProfileImpl();
        mainContainer = rt.createMainContainer(p1);

        if(SEPARATE_CONTAINERS){
            Profile p2 = new ProfileImpl();
            agentContainer = rt.createAgentContainer(p2);
        } else {
            agentContainer = mainContainer;
        }

        System.out.println("Scenario Launcher - Complex Scenario");
        launchAgents(numAGVs, ratePart, ftc, agvCap, relocAGVs);

    }

    /**
     * Launch agents for simple scenario
     */
    private void launchAgents(int numAGVs, int ratePart, boolean ftc, int agvCap, boolean relocAGVs){

        try {

            // Storage
            StorageAgent sa = new StorageAgent(190, 50, 3);
            agentContainer.acceptNewAgent("Storage " + 0, sa).start();
            drawList.add(sa);

            // AGVs
            Random rand = new Random();

            for(int i=0; i<numAGVs; i++) {

                AGVAgent agv = new AGVAgent(rand.nextInt(WINDOW_WIDTH)+1, rand.nextInt(WINDOW_HEIGHT)+1, 10, agvCap, ftc, relocAGVs);
                agentContainer.acceptNewAgent("AGV " + i, agv).start();
                drawList.add(agv);

            }

            // Process Manager A - Step 1
            ProcessMachineAgent pma1a = new ProcessMachineAgent(50, 0, 1, 500, 5);
            agentContainer.acceptNewAgent("Process Machine - 1A", pma1a).start();
            drawList.add(pma1a);

            // Process Manager B - Step 1
            ProcessMachineAgent pma1b = new ProcessMachineAgent(50, 90, 1, 500, 5);
            agentContainer.acceptNewAgent("Process Machine - 1B", pma1b).start();
            drawList.add(pma1b);

            // Process Manager A - Step 2
            ProcessMachineAgent pma2a = new ProcessMachineAgent(150, 0, 2, 500, 5);
            agentContainer.acceptNewAgent("Process Machine - 2A", pma2a).start();
            drawList.add(pma2a);

            // Process Manager A - Step 2
            ProcessMachineAgent pma2b = new ProcessMachineAgent(150, 90, 2, 500, 5);
            agentContainer.acceptNewAgent("Process Machine - 2B", pma2b).start();
            drawList.add(pma2b);

            // MANAGER
            ManagerAgent ma = new ManagerAgent(0, 50, ratePart);
            agentContainer.acceptNewAgent("Manager " + 0, ma).start();
            drawList.add(ma);

            // RMA Agent
            //rma rma = new rma();
            //agentContainer.acceptNewAgent("RMA", rma).start();

        } catch(StaleProxyException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

    }

}
