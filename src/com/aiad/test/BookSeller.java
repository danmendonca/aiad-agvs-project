package com.aiad.test;

import jade.core.Agent;
import jade.core.behaviours.*;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;


import java.util.*;
import java.util.concurrent.CyclicBarrier;

public class BookSeller extends Agent {

    // The catalogue of books for sale (maps the title of a book to its price)
    private Hashtable catalogue;

    // Put agent initializations here
    protected void setup(){

        // Create the catalogue
        catalogue = new Hashtable();

        // Add new books for sale
        catalogue.put("Book 1", 100);
        catalogue.put("Book 2", 200);
        catalogue.put("Book 3", 300);

        // Register the book-selling service in the yellow pages
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType("book-selling");
        sd.setName("JADE-book-trading");
        dfd.addServices(sd);

        try {
            DFService.register(this, dfd);
        }
        catch (FIPAException fe){
            fe.printStackTrace();
        }


        // Add the behaviour serving requests for offer from buyer agents
        addBehaviour(new OfferRequestsServer());

        // Add the behaviour serving purchase orders from buyer agents
        addBehaviour(new PurchaseOrdersServer());

    }

    //Put agent clean-up operations here
    protected void takeDown(){

        // Deregister from the yellow pages
        try{
            DFService.deregister(this);
        } catch(FIPAException fe){
            fe.printStackTrace();
        }

        // Printout a dismissal message
        System.out.println("Seller-agent " + getAID().getName() + " terminating.");
    }

    /**
     Inner class OfferRequestsServer
     This is the behaviour used by Book-seller agents to serve incoming requests
     for offer from buyer agents.
     If the requested book is in the local catalogue the seller agent replies
     with a PROPOSE message specifying the price. Otherwise a REFUSE message is
     sent back.
     */
    private class OfferRequestsServer extends CyclicBehaviour {

        @Override
        public void action() {

            // Selecting messages with given characteristics from the message queue
            MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.CFP);
            ACLMessage msg = myAgent.receive(mt);
            
            if(msg != null){
                
                // Message received. Process it
                String title = msg.getContent();
                ACLMessage reply = msg.createReply();
                
                Integer price = (Integer) catalogue.get(title);
                
                if(price != null){
                    
                    // The requested book is available for sale
                    // Reply with the price
                    reply.setPerformative(ACLMessage.PROPOSE);
                    reply.setContent(String.valueOf(price.intValue()));
                    
                } else {
                    
                    // the requested book is NOT available for sale
                    reply.setPerformative(ACLMessage.REFUSE);
                    reply.setContent("not-available");                    
                }
                
                myAgent.send(reply);
                
            } else {

                /**
                 * The agent’s thread starts a continuous loop that is extremely CPU consuming.
                 * In order to avoid that we would like to execute the action() method of the OfferRequestsServer behaviour
                 * only when a new message is received
                 */

                block();
            }
            

        }
    } // End of inner class OfferRequestedServer

    /**
        Inner class PurchaseOrdersServer
        This is the behaviour used by Book-seller agents to serve incoming
        offer acceptances (i.e. purchase orders) from buyer agents.
        The seller agent removes the purchased book from its catalogue
        and replies with an INFORM message to notify the buyer that the
        purchase has been successfully completed.
     */
    private class PurchaseOrdersServer extends CyclicBehaviour {

        @Override
        public void action(){

            MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.ACCEPT_PROPOSAL);
            ACLMessage msg = myAgent.receive();

            if(msg != null){

                // ACCEPT_PROPOSAL Message received. Process it
                String title = msg.getContent();
                ACLMessage reply = msg.createReply();

                Integer price = (Integer) catalogue.remove(title);

                if(price != null){
                    reply.setPerformative(ACLMessage.INFORM);
                    System.out.println(title + " sold to agent " + msg.getSender().getName());
                } else {

                    // The requested book has been sold to another buyer in the meanwhile
                    reply.setPerformative(ACLMessage.FAILURE);
                    reply.setContent("not-available");
                }

                myAgent.send(reply);
            } else {
                block();
            }
        }

    }

}
