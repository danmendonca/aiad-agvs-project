package com.aiad.behaviours;

import jade.core.AID;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public final class BehaviourHelper {

    public static final String PART_PROCESS = "part-process";

    private BehaviourHelper() {

    }


    public static void testMethod() {
        System.out.println("something");
    }

    /**
     * @param serviceDescription
     * @param msgBefore          a message to display before getting the agents from the yellow pages
     * @param msgAfter           a message to display after checking the yellow pages for agents of the service
     * @return
     */
    public static List<AID> getAgentsOfService(Agent agent, String serviceDescription, String msgBefore, String msgAfter)
            throws FIPAException {
        DFAgentDescription template = new DFAgentDescription();
        ServiceDescription sd = new ServiceDescription();
        sd.setType(serviceDescription);
        template.addServices(sd);

        if (msgBefore != null) System.out.println(agent.getLocalName() + " - " + msgBefore);

        // Search yellow pages
        DFAgentDescription[] result = DFService.search(agent, template);

        if (msgAfter != null) System.err.println(msgAfter);

        List<AID> candidatesForPart = Arrays.stream(result)
                .map(r -> {
                    System.out.println(r.getName().getLocalName());
                    return r.getName();
                }).collect(Collectors.toList());

        System.out.println("------------------------");

        return candidatesForPart;


    }
}
