package com.aiad.behaviours.Machines;

import com.aiad.agents.Part;
import com.aiad.agents.ProcessMachineAgent;
import jade.core.behaviours.TickerBehaviour;

public class ProcessBehaviour extends TickerBehaviour {
    ProcessMachineAgent myAgent;
    boolean hadItPreviously;

    public ProcessBehaviour(ProcessMachineAgent a, long period) {
        super(a, period);
        myAgent = a;
        hadItPreviously = false;
    }

    @Override
    protected void onTick() {
        if (hadItPreviously) {
            Part part = myAgent.removeHeadPart(); //getToProcessParts().remove(0);
            myAgent.addPartToProcessed(part);  //.getProcessedParts().add(part);

            myAgent.addBehaviour(new AskForMachineBehaviour(myAgent, part));

            if (myAgent.getToProcessPartsSize() < 1)
                hadItPreviously = false;
        } else {
            if (myAgent.getToProcessPartsSize() >= 1)
                hadItPreviously = true;
        }
    }
}
