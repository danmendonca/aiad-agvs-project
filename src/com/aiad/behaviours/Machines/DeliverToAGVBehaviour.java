package com.aiad.behaviours.Machines;


import com.aiad.agents.FactoryAgent;
import com.aiad.agents.Part;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;


/**
 * Receives an INFORM ACLMessage from an AGV to deliver/pick a Part that the agent of this behaviour is holding
 */
public class DeliverToAGVBehaviour extends CyclicBehaviour {

    FactoryAgent myAgent;

    public DeliverToAGVBehaviour(FactoryAgent myAgent) {
        super(myAgent);
        this.myAgent = myAgent;
    }

    @Override
    public void action() {
        MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
        MessageTemplate mt2 = MessageTemplate.MatchConversationId("part-pickup");
        mt = MessageTemplate.and(mt, mt2);

        ACLMessage agvMsg = myAgent.receive(mt);

        if (agvMsg != null) {
            //if (agvMsg.getPerformative() == ACLMessage.INFORM) {

                Part part = Part.convertToPart(agvMsg.getContent());

            //if (agvMsg.getConversationId().equalsIgnoreCase("part-pickup"))
                    myAgent.deliverPart(part);
                /*else {
                    myAgent.postMessage(agvMsg);
                }*/
            // }
        } else {
            block();
        }
    }
}
