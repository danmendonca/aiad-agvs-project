package com.aiad.behaviours.Machines;

import com.aiad.agents.FactoryAgent;
import com.aiad.agents.Part;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class ReceiveFromAGVBehaviour extends CyclicBehaviour {

    FactoryAgent myAgent;

    public ReceiveFromAGVBehaviour(FactoryAgent myAgent) {
        super(myAgent);
        this.myAgent = myAgent;
    }

    @Override
    public void action() {

        MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
        MessageTemplate mt2 = MessageTemplate.MatchConversationId("part-dropoff");
        mt = MessageTemplate.and(mt, mt2);
        ACLMessage agvMsg = myAgent.receive(mt);

        if (agvMsg != null) {
            //if (agvMsg.getPerformative() == ACLMessage.INFORM) {

            // if (agvMsg.getConversationId().equalsIgnoreCase("part-dropoff")) {
            Part part = Part.convertToPart(agvMsg.getContent());

            // Update Part Location Info
            part.origLocalName = myAgent.getLocalName();
            part.partX = myAgent.getxPos();
            part.partY = myAgent.getyPos();

            myAgent.receivePart(part);

                    ACLMessage reply = agvMsg.createReply();
                    reply.setPerformative(ACLMessage.CONFIRM);
                    myAgent.send(reply);

               /* } else {
                    myAgent.postMessage(agvMsg);
                }*/
            // }
        } else {
            block();
        }
    }
}
