package com.aiad.behaviours.Machines;

import com.aiad.agents.ProcessMachineAgent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

/**
 * Answers cfps to handle a part coming from a previous stage in the production line
 */
public class ProposalBehaviour extends CyclicBehaviour {

    ProcessMachineAgent myAgent;

    public ProposalBehaviour(ProcessMachineAgent agent) {
        this.myAgent = agent;
    }

    @Override
    public void action() {
        // Selecting messages with given characteristics from the message queue
        MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.CFP);
        ACLMessage msg = myAgent.receive(mt);

        if (msg != null) {

            ACLMessage reply = msg.createReply();

            if (myAgent.getCapacity() > 0) {
                reply.setPerformative(ACLMessage.PROPOSE);

                long maxTimeToProcess = myAgent.proposeTime();
                String replyMsg = String.valueOf(maxTimeToProcess) + "-"
                        + myAgent.getxPos() + "-" + myAgent.getyPos();
                reply.setContent(replyMsg);
            } else {
                //we don't have capacity to hold more parts
                reply.setPerformative(ACLMessage.REFUSE);
                reply.setContent("not-available");
            }

            myAgent.send(reply);

        } else {

            block();
        }

    }
}
