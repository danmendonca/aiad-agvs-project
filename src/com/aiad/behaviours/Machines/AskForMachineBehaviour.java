package com.aiad.behaviours.Machines;

import com.aiad.agents.FactoryAgent;
import com.aiad.agents.Part;
import com.aiad.behaviours.BehaviourHelper;
import com.aiad.ui.ScenarioLauncher;
import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.util.List;


public class AskForMachineBehaviour extends Behaviour {
    public List<AID> candidatesForPart;
    AID bestMachine;
    Part part;
    FactoryAgent myAgent;

    String serviceDescription;


    boolean foundMachine;
    int bestTimeTillFinish, step, repliesCnt, bestMachX, bestMachY;
    MessageTemplate mt;

    public AskForMachineBehaviour(FactoryAgent a, Part part) {
        super(a);
        myAgent = a;
        this.step = 0;
        this.foundMachine = false;
        this.bestTimeTillFinish = Integer.MAX_VALUE;
        this.part = part;


    }

    @Override
    public void action() {

        switch (step) {

            case 0:
                getMachines();
                break;
            case 1:
                // Send the cfp to all next step machines
                askForProposals();
                break;

            case 2:
                // Receive all proposals/refusals from next step machines
                selectBestProposal();
                break;

            case 3:
                // Accept the best proposal
                acceptBestProposal();
                break;

            case 4:
                // Receive the confirmation that proposal still stands
                getConfirmation();
                break;
        }
    }

    private void getMachines() {
        String msgBefore = "Searching for next step machines:";
        String msgAfter = "Found the following next step machines:";

        String serviceDescription = BehaviourHelper.PART_PROCESS + Integer.toString(myAgent.getStep() + 1);
        try {
            candidatesForPart = BehaviourHelper.getAgentsOfService(myAgent, serviceDescription, msgBefore, msgAfter);
            if (candidatesForPart.size() > 0)
                step++;
        } catch (FIPAException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean done() {
        return (foundMachine);
    }

    private void askForProposals() {
        ACLMessage cfp = new ACLMessage(ACLMessage.CFP);

        //candidatesForPart.forEach(candidate-> cfp.addReceiver(candidate));
        candidatesForPart.forEach(cfp::addReceiver);

        String conversationId = "askForMachineProposals_" + System.currentTimeMillis();

        cfp.setContent(part.toString());
        cfp.setConversationId(conversationId);
        cfp.setReplyWith("cfp" + System.currentTimeMillis()); // Unique value
        myAgent.send(cfp);

        //Prepare the template to get proposals
        mt = MessageTemplate.and(MessageTemplate.MatchConversationId(conversationId),
                MessageTemplate.MatchInReplyTo(cfp.getReplyWith()));
        step = 2;
    }

    private void selectBestProposal() {

        ACLMessage reply = myAgent.receive(mt);

        if (reply != null) {
            // Reply received
            if (reply.getPerformative() == ACLMessage.PROPOSE) {

                // This is an offer, and its content is the time it will take untill the part is ready
                String[] parsedReply = reply.getContent().split("-");
                int timeTillFinishing = Integer.parseInt(parsedReply[0]);

                if (bestMachine == null || timeTillFinishing < bestTimeTillFinish) {

                    // This is the best offer at present
                    bestTimeTillFinish = timeTillFinishing;
                    bestMachX = Integer.parseInt(parsedReply[1]);
                    bestMachY = Integer.parseInt(parsedReply[2]);
                    bestMachine = reply.getSender();
                }

            }
            repliesCnt++;

            // We received all replies
            if (repliesCnt >= candidatesForPart.size()) {
                step = (bestMachine != null) ? 3 : 0;
            }
        } else {
            block();
        }
    }

    private void acceptBestProposal() {
        ACLMessage order = new ACLMessage(ACLMessage.ACCEPT_PROPOSAL);
        order.addReceiver(bestMachine);
        String conversationID = "acceptMachineProposal_" + System.currentTimeMillis();

        part.proposedTime = bestTimeTillFinish;

        if (ScenarioLauncher.DEBUG_MODE && bestMachine == null) {
            System.out.println("==========DEBUG PRINT==========");
            System.out.println("Accepted proposal without best machine");
        }

        order.setContent(part.toString());
        order.setConversationId(conversationID);
        order.setReplyWith("order" + System.currentTimeMillis());
        myAgent.send(order);

        // Prepare the template to get the purchase order reply
        mt = MessageTemplate.and(MessageTemplate.MatchConversationId(conversationID),
                MessageTemplate.MatchInReplyTo(order.getReplyWith()));
        step = 4;
    }

    private void getConfirmation() {
        ACLMessage reply = myAgent.receive(mt);

        if (reply != null) {
            // Purchase order reply received
            if (reply.getPerformative() == ACLMessage.INFORM) {

                // Save Destination Machine Info on Part
                part.destLocalName = bestMachine.getLocalName();
                part.destX = bestMachX;
                part.destY = bestMachY;

                // Next Machine set for the part.
                System.out.println(myAgent.getLocalName() + ":");
                System.out.println("Part " + part.partId + " successfully arranged.");
                System.out.println("time until next process finishes = " + bestTimeTillFinish);
                System.out.println("------------------------");

                myAgent.addBehaviour(new AskForAGVBehaviour(myAgent, part));
                foundMachine = true;
            } else {
                step = 0;
                bestMachine = null;
            }
        } else {
            block();
        }
    }

}
