package com.aiad.behaviours.Machines;

import com.aiad.agents.FactoryAgent;
import com.aiad.agents.Part;
import com.aiad.behaviours.BehaviourHelper;
import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.util.List;


public class AskForAGVBehaviour extends Behaviour {

    FactoryAgent myAgent;
    List<AID> candidatesForPicking;
    Part part;
    AID bestAGV;
    boolean foundAGV;
    Double bestDistance;
    int repliesCnt;
    MessageTemplate mt;
    int step;
    String serviceDescription;

    public AskForAGVBehaviour(FactoryAgent myAgent, Part part) {
        this.myAgent = myAgent;
        this.part = part;
        bestAGV = null;
        step = 0;
        foundAGV = false;

        serviceDescription = "part-transport";

    }

    @Override
    public void action() {

        switch (step) {
            case 0:
                findAGVs();
                break;
            case 1:
                askForProposals();
                break;
            case 2:
                selectBestProposals();
                break;
            case 3:
                acceptBestProposal();
                break;
            case 4:
                getConfirmation();
                break;
            default:
                break;
        }
    }

    @Override
    public boolean done() {
        return foundAGV;
    }

    private void findAGVs() {
        String msgBefore = "Searching for agvs:";
        String msgAfter = "Found the following agvs:";

        candidatesForPicking = null;
        try {
            candidatesForPicking = BehaviourHelper.getAgentsOfService(myAgent, serviceDescription, msgBefore, msgAfter);

            if (candidatesForPicking.size() > 0)
                step = 1;

        } catch (FIPAException e) {
            e.printStackTrace();
            System.out.println("Could not initiate negotiation with agvs");
            foundAGV = true;
        }
    }

    private void askForProposals() {
        ACLMessage cfp = new ACLMessage(ACLMessage.CFP);

        candidatesForPicking.forEach(cfp::addReceiver);

        String conversationId = "cfp_part-transport_" + System.currentTimeMillis();
        //cfp.setContentObject(part);
        cfp.setContent(myAgent.getxPos() + "-" + myAgent.getyPos());

        cfp.setConversationId(conversationId);
        cfp.setReplyWith("cfp" + System.currentTimeMillis()); // Unique value
        myAgent.send(cfp);

        //Prepare the template to get proposals
        mt = MessageTemplate.and(MessageTemplate.MatchConversationId(conversationId),
                MessageTemplate.MatchInReplyTo(cfp.getReplyWith()));
        step = 2;
    }

    private void selectBestProposals() {
        ACLMessage reply = myAgent.receive(mt);

        if (reply != null) {
            // Reply received
            if (reply.getPerformative() == ACLMessage.PROPOSE) {

                // This is an offer, and its content is the time it will take to the agv get here
                Double distance = Double.parseDouble(reply.getContent());

                // This is the best offer at present
                if (bestAGV == null || distance < bestDistance) {
                    bestDistance = distance;
                    bestAGV = reply.getSender();
                }

            }
            repliesCnt++;

            if (repliesCnt >= candidatesForPicking.size()) {
                step = (bestAGV != null) ? 3 : 0;
            }

        } else {
            block();
        }
    }

    private void acceptBestProposal() {
        ACLMessage order = new ACLMessage(ACLMessage.ACCEPT_PROPOSAL);
        order.addReceiver(bestAGV);
        String conversationID = "AcP_part-transport_" + System.currentTimeMillis();

        order.setContent(part.toString());
        order.setConversationId(conversationID);
        order.setReplyWith("order" + System.currentTimeMillis());
        myAgent.send(order);

        // Prepare the template to get the purchase order reply
        mt = MessageTemplate.and(MessageTemplate.MatchConversationId(conversationID),
                MessageTemplate.MatchInReplyTo(order.getReplyWith()));
        step = 4;

    }

    private void getConfirmation() {
        ACLMessage reply = myAgent.receive(mt);

        if (reply != null) {
            // Purchase order reply received
            if (reply.getPerformative() == ACLMessage.INFORM) {
                // Next Machine set for the part.
                System.out.println(myAgent.getLocalName());
                System.out.println("Part " + part.partId + " has an AGV set for transportation.");
                System.out.println("distance from agv = " + bestDistance);
                System.out.println("------------------------");

                foundAGV = true;
            } else {
                step = 0;
                bestAGV = null;
            }
        } else {
            block();
        }
    }

}
