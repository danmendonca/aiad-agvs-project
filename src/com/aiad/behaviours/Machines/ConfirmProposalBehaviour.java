package com.aiad.behaviours.Machines;

import com.aiad.agents.Part;
import com.aiad.agents.ProcessMachineAgent;
import com.aiad.ui.ScenarioLauncher;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

/**
 * Behaviour that by inspecting the ACLMessage content, gets the time that previously promised that would take to have a
 * part ready and verifies if that time is still expected time. If it is, it Informs the sender of it, otherwise, sends
 * a FAILURE reply.
 */
public class ConfirmProposalBehaviour extends CyclicBehaviour {

    ProcessMachineAgent myAgent;

    public ConfirmProposalBehaviour(ProcessMachineAgent myAgent) {
        this.myAgent = myAgent;
    }

    @Override
    public void action() {
        // Selecting messages with given characteristics from the message queue
        MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.ACCEPT_PROPOSAL);
        ACLMessage msg = myAgent.receive();

        if (msg != null) {

            // ACCEPT_PROPOSAL Message received. Process it
            if (msg.getPerformative() != ACLMessage.ACCEPT_PROPOSAL) {
                myAgent.postMessage(msg);
                return;
            }


            if (ScenarioLauncher.DEBUG_MODE) {
                System.out.println("==========DEBUG PRINT==========");
                System.out.println("I'm agent: " + myAgent.getLocalName());
                System.out.println("sender: " + msg.getSender());
                System.out.println("conversationID: " + msg.getConversationId());
                System.out.println("content: " + msg.getContent());
                System.out.println("==========END PRINT==========");
            }
            Part part = Part.convertToPart(msg.getContent());

            ACLMessage reply = msg.createReply();

            long currentTimeToProcess = myAgent.proposeTime();
            if (currentTimeToProcess <= part.proposedTime && myAgent.addPart(part)) {

                reply.setPerformative(ACLMessage.INFORM);
                System.out.println(myAgent.getLocalName() +
                        " receiving part from " + msg.getSender().getLocalName());
                System.out.println("------------------------");

            } else {

                reply.setPerformative(ACLMessage.FAILURE);
                reply.setContent("cannot-do-that-time");

            }

            myAgent.send(reply);
        } else {
            block();
        }
    }
}
