package com.aiad.behaviours.AGVs;

import com.aiad.agents.AGVAgent;
import com.aiad.agents.Part;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class TransportPerformer extends TickerBehaviour {

    private AGVAgent agvAgent;
    private MessageTemplate mt;

    public TransportPerformer(AGVAgent agv, long period) {
        super(agv, period);
        this.agvAgent = agv;
    }

    /**
     *
     */
    @Override
    protected void onTick() {

        // Check if there are part transport tasks
        if(agvAgent.agvLoad.isEmpty()){

            if(agvAgent.relocAGVs && (agvAgent.getxPos() != 100 || agvAgent.getyPos() != 50)) {
                agvAgent.moveToNextPos(100, 50);
            }

            return;
        }

        // Get part transport status
        Part part = agvAgent.agvLoad.get(0);
        Part.Status partStatus = part.status;

        // AGV Autonomy Verification
        if(agvAgent.checkAutonomy){
            agvAgent.rechargeProcess(part);
            return;
        }

        switch (partStatus) {

            case WAITING_PICKUP:

                // Move to position closer to part pickup
                agvAgent.moveToNextPos(part.partX, part.partY);

                // Check if agv arrived at part pickup position
                if(agvAgent.getxPos() == part.partX && agvAgent.getyPos() == part.partY) {
                    part.status = Part.Status.PICKUP;
                }

                break;

            case PICKUP:

                // Send pickup request to process machine
                ACLMessage pickupMsg = new ACLMessage(ACLMessage.INFORM);

                pickupMsg.addReceiver(getAgent().getAID(part.origLocalName));
                pickupMsg.setContent(part.toString());
                pickupMsg.setConversationId("part-pickup");
                agvAgent.send(pickupMsg);

                // Travel to part destination
                agvAgent.checkAutonomy = true;
                part.status = Part.Status.IN_TRANSIT;

                break;

            case IN_TRANSIT:

                // Move to position closer to part pickup
                agvAgent.moveToNextPos(part.destX, part.destY);

                // Check if agv arrived at part drop off destination
                if(agvAgent.getxPos() == part.destX && agvAgent.getyPos() == part.destY) {
                    part.status = Part.Status.DROP_OFF;
                }

                break;

            case DROP_OFF:

                // Send dropoff message to process machine
                ACLMessage dropoffMsg = new ACLMessage(ACLMessage.INFORM);

                dropoffMsg.addReceiver(agvAgent.getAID(part.destLocalName));

                dropoffMsg.setContent(part.toString());
                dropoffMsg.setConversationId("part-dropoff");
                agvAgent.send(dropoffMsg);

                mt = MessageTemplate.MatchConversationId("part-dropoff");
                part.status = Part.Status.DROP_OFF_DONE;



                break;

            case DROP_OFF_DONE:

                mt = MessageTemplate.MatchPerformative(ACLMessage.CONFIRM);
                ACLMessage reply = agvAgent.receive(mt);

                if(reply != null) {

                    // Dropoff message reply received
                    System.out.println(agvAgent.getLocalName() + ": Part " +
                            part.partId + " successfully delivered.");
                    System.out.println("------------------------");

                    // Remove transport task
                    agvAgent.agvLoad.remove(0);

                    // Activate autonomy verification
                    agvAgent.checkAutonomy = true;

                }


                break;

            default:
                System.out.printf(agvAgent.getLocalName() + ": something went wrong!");
                break;

        }

    }





}
