package com.aiad.behaviours.AGVs;

import com.aiad.agents.AGVAgent;
import com.aiad.agents.Part;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class TransportOrderServer extends CyclicBehaviour {

    private AGVAgent agvAgent;

    public TransportOrderServer(AGVAgent agv) {
        super(agv);
        this.agvAgent = agv;
    }

    @Override
    public void action() {

        MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.ACCEPT_PROPOSAL);
        ACLMessage msg = agvAgent.receive(mt);

        if (msg != null) {

            // ACCEPT_PROPOSAL Message received
            Part part = Part.convertToPart(msg.getContent());

            ACLMessage reply = msg.createReply();

            // Check agv load max capacity
            if (agvAgent.agvLoad.size() < agvAgent.maxCapacity) {

                // Add part to agv load
                part.status = Part.Status.WAITING_PICKUP;
                agvAgent.agvLoad.add(part);

                // Send message to requesting machine
                reply.setPerformative(ACLMessage.INFORM);

                System.out.println("Part " + part.partId +
                        " added to " + agvAgent.getLocalName() + " load - Cap = " + agvAgent.agvLoad.size());
                System.out.println("------------------------");

            } else {

                reply.setPerformative(ACLMessage.FAILURE);
                reply.setContent("full-capacity");

            }

            agvAgent.send(reply);

        } else {
            block();
        }

    }
}
