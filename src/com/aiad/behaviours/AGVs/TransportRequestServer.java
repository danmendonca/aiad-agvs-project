package com.aiad.behaviours.AGVs;

import com.aiad.agents.AGVAgent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

/**
 * Transport Request Behaviour
 * Um AgvAgent responde a pedidos de levantamento e transporte
 * feitos por um ProcessMachineAgent, indicando a capacidade disponível
 * e tempo de deslocação até ao agente.
 */
public class TransportRequestServer extends CyclicBehaviour {

    private AGVAgent agvAgent;
    private boolean fullTransportCost;

    public TransportRequestServer(AGVAgent agv, boolean ftc) {
        super(agv);
        this.agvAgent = agv;
        this.fullTransportCost = ftc;
    }

    @Override
    public void action() {

        // Select transport request messages
        MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.CFP);
        ACLMessage msg = myAgent.receive(mt);

        if(msg != null){

            //CFP Message received
            String machinePosition = msg.getContent();
            ACLMessage reply = msg.createReply();

            if(agvAgent.agvLoad.size() < agvAgent.maxCapacity){

                // Parse requesting machine position
                String[] posArray = machinePosition.split("-");

                Double transportCost;
                if(!fullTransportCost) {
                    transportCost = agvAgent.calcTransportCost(posArray);
                } else {
                    transportCost = agvAgent.calcFullTransportCost(posArray);
                }

                // Reply the transport cost to the requesting machine
                reply.setPerformative(ACLMessage.PROPOSE);
                reply.setContent(String.valueOf(transportCost));

            } else {

                reply.setPerformative(ACLMessage.REFUSE);
                reply.setContent("full-capacity");
            }

            myAgent.send(reply);

        } else {
            block();
        }

    }
}
