package com.aiad.behaviours.Manager;

import com.aiad.agents.ManagerAgent;
import com.aiad.agents.Part;
import com.aiad.behaviours.Machines.AskForMachineBehaviour;
import jade.core.behaviours.TickerBehaviour;


public class NewPartBehaviour extends TickerBehaviour {

    ManagerAgent myAgent;

    public NewPartBehaviour(ManagerAgent a, long period) {
        super(a, period);
        this.myAgent = a;

    }

    @Override
    protected void onTick() {
        Part part = new Part(myAgent.getxPos(), myAgent.getyPos(), myAgent.getLocalName());

        myAgent.getParts().add(part);
        myAgent.addBehaviour(new AskForMachineBehaviour(myAgent, part));
    }
}
