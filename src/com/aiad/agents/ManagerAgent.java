package com.aiad.agents;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import java.util.ArrayList;

import com.aiad.behaviours.BehaviourHelper;
import com.aiad.behaviours.Machines.DeliverToAGVBehaviour;
import com.aiad.behaviours.Manager.NewPartBehaviour;

public class ManagerAgent extends FactoryAgent {


    String msgBefore, serviceDesc, msgAfter;
    private ArrayList<Part> parts;

    //JAVAFX
    final Rectangle agentShape;


    public ManagerAgent(int xPos, int yPos, long rate) {
        super(xPos, yPos, 0, rate);
        this.serviceDesc = BehaviourHelper.PART_PROCESS + "1";
        this.msgBefore = "Searching for next step machines:";
        this.msgAfter = "Found the following next step machines:";
        this.parts = new ArrayList<>();

        // Agent shape
        agentShape = new Rectangle(xPos, yPos, 10, 10);
        agentShape.setFill(Color.GREEN);
    }

    protected void setup() {

        System.out.println(getLocalName() + ": ready!");
        System.out.println("------------------------");

        // Add Ticker Behaviour that requests a new part to processing machines
        addBehaviour(new NewPartBehaviour(this, this.rate));

        // Add Cyclic Behaviour that processes the pickup request from agv agents
        addBehaviour(new DeliverToAGVBehaviour(this));

    }

    protected void takeDown() {
        System.out.println(getLocalName() + ": terminating.");
        System.out.println("------------------------");
    }

    @Override
    public Shape getAgentShape() {
        return this.agentShape;
    }

    public String getMsgBefore() {
        return msgBefore;
    }

    public String getServiceDesc() {
        return serviceDesc;
    }

    public String getMsgAfter() {
        return msgAfter;
    }

    public ArrayList<Part> getParts() {
        return parts;
    }

    @Override
    public boolean deliverPart(Part part) {
        return parts.remove(part);
    }

    @Override
    public boolean receivePart(Part part) {
        return false;
    }

    @Override
    public boolean addPart(Part part) {
        getParts().add(part);
        return true;
    }
}
