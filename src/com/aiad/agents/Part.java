package com.aiad.agents;

import com.google.gson.Gson;

import java.io.Serializable;

public class Part implements Serializable {

    // Part ID
    private static int id_counter = 0;
    public final int partId;

    // Status
    public Status status;

    // Origin and Destination
    public Integer partX;
    public Integer partY;
    public Integer destX;
    public Integer destY;

    public String origLocalName;
    public String destLocalName;

    // Process Machine Data
    public long proposedTime;

    public Part(int pX, int pY, String origLocalName) {
        this.partId = id_counter++;

        this.partX = pX;
        this.partY = pY;

        this.origLocalName = origLocalName;
        this.status = Status.WAITING_PROCESSING;
    }

    public static Part convertToPart(String json) {
        Gson gson = new Gson();
        Part part = null;

        try {
            part = gson.fromJson(json, Part.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return part;
    }

    @Override
    public boolean equals(Object obj) {

        if (!(obj instanceof Part))
            return false;

        else
            return (((Part) obj).partId == this.partId);
    }

    public Status getStatus() {
        return status;
    }

    public int getPartId() {
        return partId;
    }

    public Integer getPartX() {
        return partX;
    }

    public Integer getPartY() {
        return partY;
    }

    public Integer getDestX() {
        return destX;
    }

    public Integer getDestY() {
        return destY;
    }

    public String getOrigLocalName() {
        return origLocalName;
    }

    public String getDestLocalName() {
        return destLocalName;
    }

    @Override
    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public enum Status {
        WAITING_PICKUP,
        PICKUP,
        IN_TRANSIT,
        DROP_OFF,
        DROP_OFF_DONE,
        WAITING_PROCESSING,
        PROCESSED
    }
}
