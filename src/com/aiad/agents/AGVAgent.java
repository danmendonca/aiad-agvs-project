package com.aiad.agents;

// JADE
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;

// JAVA
import java.util.ArrayList;

import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javafx.scene.shape.Circle;

import com.aiad.behaviours.AGVs.TransportOrderServer;
import com.aiad.behaviours.AGVs.TransportPerformer;
import com.aiad.behaviours.AGVs.TransportRequestServer;
import com.aiad.ui.AgentDrawable;


public class AGVAgent extends Agent implements AgentDrawable {

    // AGV Recharging process
    public static final int RECHARGING_X_COORDINATE = 105;
    public static final int RECHARGING_Y_COORDINATE = 25;
    public int maxBatteryLevel = 1000;
    public int batteryLevel = 1000;
    public boolean checkAutonomy = true;
    public boolean fullTransportCost;
    public boolean relocAGVs;

    private int xPos = 0;
    private int yPos = 0;

    public int velocity;
    public int maxCapacity;
    public ArrayList<Part> agvLoad;

    final Circle agentShape;

    public final int getxPos() {
        return xPos;
    }

    public final int getyPos() {
        return yPos;
    }

    public Shape getAgentShape() {

        return agentShape;
    }

    public final void setXPos(int value) {
        this.xPos = value;
    }

    public final void setYPos(int value) {
        this.yPos = value;
    }

    public AGVAgent(int xPos, int yPos, int veloc, int maxCapacity, boolean ftc, boolean relocAGVs) {

        this.setXPos(xPos);
        this.setYPos(yPos);
        this.velocity = veloc;
        this.maxCapacity = maxCapacity;
        this.fullTransportCost = ftc;
        this.relocAGVs = relocAGVs;
        agvLoad = new ArrayList<>();

        // Agent shape
        this.agentShape = new Circle(xPos, yPos, 5, Color.GREEN);

    }

    public void rechargeProcess(Part transportPart) {

        if(transportPart.status == Part.Status.WAITING_PICKUP) {

            // WAITING_PICKUP
            Double tripDist = getLinearDist(getxPos(), getyPos(), transportPart.partX, transportPart.partY)
                    + getLinearDist(transportPart.partX, transportPart.partY, RECHARGING_X_COORDINATE, RECHARGING_Y_COORDINATE);

            //System.out.println(getLocalName() + " - Battery Level: " + batteryLevel);
            //System.out.println(getLocalName() + " - Trip Dist: " + tripDist);

            if(tripDist < (double)batteryLevel) {
                checkAutonomy = false;
                System.out.println(getLocalName() + " - Battery Level: OK");
                System.out.println("------------------------");
                return;
            }

            if(getxPos() == RECHARGING_X_COORDINATE && getyPos() == RECHARGING_Y_COORDINATE) {

                batteryLevel = maxBatteryLevel;
                checkAutonomy = false;
                System.out.println(getLocalName() + " - Battery Recharge");
                System.out.println("------------------------");
                return;

            }

            moveToNextPos(RECHARGING_X_COORDINATE, RECHARGING_Y_COORDINATE);

        } else if (transportPart.status == Part.Status.IN_TRANSIT) {

            // IN_TRANSIT

            Double tripDist = getLinearDist(getxPos(), getyPos(), transportPart.destX, transportPart.destY)
                    + getLinearDist(transportPart.partX, transportPart.partY, RECHARGING_X_COORDINATE, RECHARGING_Y_COORDINATE);

            //System.out.println(getLocalName() + " - Battery Level: " + batteryLevel);
            //System.out.println(getLocalName() + " - Trip Dist: " + tripDist);

            if(tripDist < (double)batteryLevel) {
                checkAutonomy = false;
                System.out.println(getLocalName() + " - Battery Level: OK");
                System.out.println("------------------------");
                return;
            }

            if(getxPos() == RECHARGING_X_COORDINATE && getyPos() == RECHARGING_Y_COORDINATE) {

                batteryLevel = maxBatteryLevel;
                checkAutonomy = false;
                System.out.println(getLocalName() + " - Battery Recharge");
                System.out.println("------------------------");
                return;

            }

            moveToNextPos(RECHARGING_X_COORDINATE, RECHARGING_Y_COORDINATE);

        } else {

            System.out.println(getLocalName() + " something went wrong in recharge process.");

        }

    }
    protected void setup() {

        System.out.println(getLocalName() + ": ready!");
        System.out.println("------------------------");

        // Register AGV Services in the yellow pages
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());

        ServiceDescription sd = new ServiceDescription();
        sd.setType("part-transport");
        sd.setName("AIAD-Parts-Factory");
        dfd.addServices(sd);

        try {
            DFService.register(this, dfd);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }

        System.out.println(getLocalName() +
                ": transport service registered.");
        System.out.println("------------------------");

        // Add behaviour for serving transport requests from process machines
        addBehaviour(new TransportRequestServer(this, fullTransportCost));

        // Add behaviour for serving transport orders from process machines
        addBehaviour(new TransportOrderServer(this));

        // Add behaviour for part transport
        addBehaviour(new TransportPerformer(this, velocity));

    }

    protected void takeDown() {

        // Deregister from the yellow pages
        try {
            DFService.deregister(this);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }

        // Printout a dismissal message
        System.out.println(getLocalName() + ": terminating.");
        System.out.println("------------------------");
    }

    /**
     *
     * @param destX
     * @param destY
     */
    public void moveToNextPos(Integer destX, Integer destY) {

        int w = destX - this.getxPos() ;
        int h = destY - this.getyPos();

        int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0 ;

        if (w<0) dx1 = -1 ; else if (w>0) dx1 = 1 ;
        if (h<0) dy1 = -1 ; else if (h>0) dy1 = 1 ;
        if (w<0) dx2 = -1 ; else if (w>0) dx2 = 1 ;

        int longest = Math.abs(w) ;
        int shortest = Math.abs(h) ;

        if (!(longest>shortest)) {
            longest = Math.abs(h) ;
            shortest = Math.abs(w) ;
            if (h<0) dy2 = -1 ; else if (h>0) dy2 = 1 ;
            dx2 = 0 ;
        }

        int numerator = longest >> 1 ;

        numerator += shortest;

        if (!(numerator<longest)) {

            this.setXPos(this.getxPos() + dx1);
            this.setYPos(this.getyPos() + dy1);

        } else {

            this.setXPos(this.getxPos() + dx2);
            this.setYPos(this.getyPos() + dy2);

        }

        // Decrement battery level
        this.batteryLevel--;

    }

    private Double getLinearDist(int p1X, int p1Y, int p2X, int p2Y) {

        return Math.sqrt(
                Math.pow(Math.abs(p1X - p2X), 2)
                        +
                        Math.pow(Math.abs(p1Y - p2Y), 2)
        );

    }

    public Double calcTransportCost(String[] posArray) {

        return Math.sqrt(
                Math.pow(Math.abs(Integer.parseInt(posArray[0]) - this.getxPos()), 2)
                        +
                        Math.pow(Math.abs(Integer.parseInt(posArray[1]) - this.getyPos()), 2)
        );

    }

    public Double calcFullTransportCost(String[] posArray) {

        Double totalTransportCost;

        // AGV -> Requesting Machine
        totalTransportCost = Math.sqrt(
                Math.pow(Math.abs(Integer.parseInt(posArray[0]) - this.getxPos()), 2)
                        +
                        Math.pow(Math.abs(Integer.parseInt(posArray[1]) - this.getyPos()), 2)
        );

        for(Part p : agvLoad) {

            totalTransportCost = totalTransportCost +
                    getLinearDist(p.partX, p.partY, p.destX, p.destY);

        }

        return totalTransportCost;

    }



}
