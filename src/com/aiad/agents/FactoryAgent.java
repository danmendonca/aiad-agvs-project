package com.aiad.agents;

import jade.core.Agent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

import java.util.ArrayList;

import com.aiad.ui.AgentDrawable;

public abstract class FactoryAgent extends Agent implements AgentDrawable {

    //JAVAFX
    final Rectangle agentShape;

    int xPos, yPos, step;
    long rate;
    ArrayList<Part> parts;

    public FactoryAgent(int xPos, int yPos, int step, long rate) {
        this.xPos = xPos;
        this.yPos = yPos;
        this.step = step;
        this.rate = rate;
        this.parts = new ArrayList<>();

        // Agent shape
        agentShape = new Rectangle(xPos, yPos, 10, 10);
        agentShape.setFill(Color.BLUE);

    }

    public FactoryAgent(int xPos, int yPos) {
        this.xPos = xPos;
        this.yPos = yPos;
        this.step = -1;
        this.rate = Long.MAX_VALUE;
        this.parts = new ArrayList<>();

        // Agent shape
        agentShape = new Rectangle(xPos, yPos, 10, 10);
        agentShape.setFill(Color.BLUE);
    }

    public Shape getAgentShape() {
        return agentShape;
    }

    public int getxPos() {
        return xPos;
    }

    public int getyPos() {
        return yPos;
    }

    public int getStep() {
        return step;
    }

    public long getRate() {
        return rate;
    }

    protected ArrayList<Part> getParts() {
        return parts;
    }

    public abstract boolean deliverPart(Part part);

    public abstract boolean receivePart(Part part);

    public abstract boolean addPart(Part part);

}
