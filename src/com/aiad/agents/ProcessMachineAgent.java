package com.aiad.agents;

import com.aiad.behaviours.BehaviourHelper;
import com.aiad.behaviours.Machines.*;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;

import java.util.ArrayList;

public class ProcessMachineAgent extends FactoryAgent {

    private ArrayList<Part> toProcessParts;
    private ArrayList<Part> processedParts;

    private int maxCapacity;
    private int capacity;

    public ProcessMachineAgent(int xPos, int yPos, int step, long processingRate, int maxCapacity) {
        super(xPos, yPos, step, processingRate);
        this.maxCapacity = maxCapacity;
        this.capacity = maxCapacity;
        this.processedParts = new ArrayList<>();
        this.toProcessParts = new ArrayList<>();
    }

    protected void setup() {

        // Printout a welcome message
        System.out.println(getLocalName() + ": ready!");
        System.out.println("------------------------");

        // Register Part Processing Service in the yellow pages
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType(BehaviourHelper.PART_PROCESS + getStep());
        sd.setName("AIAD-Parts-Factory");
        dfd.addServices(sd);

        try {
            DFService.register(this, dfd);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }

        System.out.println(getLocalName() + ": process service registered.");
        System.out.println("------------------------");

        addBehaviour(new ProposalBehaviour(this));
        addBehaviour(new ConfirmProposalBehaviour(this));
        addBehaviour(new ProcessBehaviour(this, this.getRate()));
        addBehaviour(new DeliverToAGVBehaviour(this));
        addBehaviour(new ReceiveFromAGVBehaviour(this));

    }

    protected void takeDown() {
        // Deregister from the yellow pages
        try {
            DFService.deregister(this);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }

        // Printout a dismissal message
        System.out.println(getLocalName() + ": terminating.");
        System.out.println("------------------------");
    }

    /**
     * @return
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     *
     * @return
     */
    public int getMaxCapacity() {
        return maxCapacity;
    }

    /**
     *
     * @return
     */
    protected ArrayList<Part> getProcessedParts() {
        return processedParts;
    }

    /**
     *
     * @return
     */
    protected ArrayList<Part> getToProcessParts() {
        return toProcessParts;
    }

    /**
     *
     * @return
     */
    protected synchronized boolean increaseCapacity() {
        boolean success = false;
        if (getCapacity() < getMaxCapacity()) {
            this.capacity++;
            success = true;
        }

        return success;
    }

    /**
     *
     * @return
     */
    protected synchronized boolean decreaseCapacity() {
        boolean success = false;
        if (capacity > 0) {
            capacity--;
            success = true;
        }

        return success;
    }

    /**
     *
     * @param part
     * @return
     */
    @Override
    public boolean deliverPart(Part part) {
        boolean deliver = getProcessedParts().remove(part);
        if (deliver) this.increaseCapacity();

        return deliver;
    }

    /**
     * Receives a part from an AGV. If a place was promised to that part, changes it from the Parts list to
     * the processing list
     * @param part
     * @return
     */
    @Override
    public boolean receivePart(Part part) {

        int index = parts.indexOf(part);
        if (index != -1) {
            parts.remove(index);
            toProcessParts.add(part);
            return true;
        }

        return false;
    }

    /**
     * Promises to save a place to store a part
     * @param part the part that will be waiting to receive from an AGV
     * @return
     */
    @Override
    public boolean addPart(Part part) {
        boolean canAddPart = decreaseCapacity();

        if (canAddPart)
            getParts().add(part);

        return canAddPart;
    }

    /**
     *
     * @return the number of the parts that were promised a place
     */
    public int getPartsSize() {
        return getParts().size();
    }

    /**
     *
     * @return the number of parts currently in the process line
     */
    public int getToProcessPartsSize() {
        return getToProcessParts().size();
    }

    /**
     * removes the head part from the process line
     * @return the head part that was in the process line
     */
    public Part removeHeadPart() {
        Part part = getToProcessParts().remove(0);
        return part;
    }

    /**
     * Adds a part that was already processed to the list of finished parts
     * @param part
     */
    public void addPartToProcessed(Part part) {
        getProcessedParts().add(part);
    }


    public long proposeTime() {
        int placeInQueue = getPartsSize() + getToProcessPartsSize() + 1;
        long maxTimeToProcess = placeInQueue * getRate();
        return maxTimeToProcess;
    }
}
