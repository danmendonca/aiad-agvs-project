package com.aiad.agents;

import com.aiad.behaviours.BehaviourHelper;
import com.aiad.behaviours.Machines.ConfirmProposalBehaviour;
import com.aiad.behaviours.Machines.ProposalBehaviour;
import com.aiad.behaviours.Machines.ReceiveFromAGVBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

public class StorageAgent extends ProcessMachineAgent {

    //JAVAFX
    final Rectangle agentShape;

    public StorageAgent(int xPos, int yPos, int step) {
        super(xPos, yPos, step, 0, Integer.MAX_VALUE);

        // Agent shape
        agentShape = new Rectangle(xPos, yPos, 10, 10);
        agentShape.setFill(Color.RED);
    }

    protected void setup() {
        // Printout a welcome message
        System.out.println(getLocalName() + ": ready!");
        System.out.println("------------------------");

        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType(BehaviourHelper.PART_PROCESS + getStep());
        sd.setName("AIAD-Parts-Factory");
        dfd.addServices(sd);

        try {
            DFService.register(this, dfd);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }

        System.out.println(getLocalName() + ": storage service registered.");
        System.out.println("------------------------");

        addBehaviour(new ProposalBehaviour(this));
        addBehaviour(new ConfirmProposalBehaviour(this));
        addBehaviour(new ReceiveFromAGVBehaviour(this));

    }

    protected void takeDown() {
        // Deregister from the yellow pages
        try {
            DFService.deregister(this);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }

        // Printout a dismissal message
        System.out.println(getLocalName() + ": terminating.");
        System.out.println("------------------------");
    }

    @Override
    public Shape getAgentShape() {
        return this.agentShape;
    }

    @Override
    public boolean deliverPart(Part part) {
        return false;
    }

    @Override
    public boolean receivePart(Part part) {
        return parts.add(part);
    }

    @Override
    public boolean addPart(Part part) {
        boolean notRepeated = getParts().remove(part);
        if (!notRepeated) {
            notRepeated = true;
            getParts().add(part);
        }

        return notRepeated;
    }

    @Override
    public long proposeTime() {
        return 0;
    }
}
